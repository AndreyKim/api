<?php

use App\Models\Client;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(Client::class, function (Faker $faker, $attributes) {
    $name = $faker->name;
    $dataFieldsCount = rand(1, 3);
    $possibleDataFields = Client::DATA_FIELDS;
    $possibleHobbies = config('hobbies');
    $possibleHobbiesCount = count($possibleHobbies);
    $dataFields = [];

    $genders = ['male', 'female'];

    for ($i = 1; $i <= $dataFieldsCount; $i++) {
        shuffle($possibleDataFields);
        $dataFields[] = array_pop($possibleDataFields);
    }
    $data = [];
    foreach ($dataFields as $field) {
        switch ($field) {
            case Client::FIELD_GENDER: {
                $data[$field] = $genders[array_rand($genders)];
                break;
            }
            case Client::FIELD_DATE_OF_BIRTH: {
                $data[$field] = Carbon::now()->subYears(rand(10, 30))->subDays(rand(1, 30))->subMonths(1, 11)->format('Y-m-d');
                break;
            }
            case Client::FIELD_HOBBY: {
                $hobbbiesCount = rand(1, $possibleHobbiesCount);
                $clientHobbies = $possibleHobbies;
                $keys = array_keys($clientHobbies);
                shuffle($keys);
                $clientHobbies = array_slice($keys, 0, $hobbbiesCount);
                $data[$field] = $clientHobbies;
            }
            default: break;
        }
    }

    $point = DB::select("SELECT ST_GeomFromEWKT('SRID=4326;POINT($faker->longitude $faker->latitude)')")[0]->st_geomfromewkt;

    return [
        'name' => $name,
        'data' => $data,
        'geo_location' => $point
    ];
});