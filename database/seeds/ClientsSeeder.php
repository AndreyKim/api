<?php

use App\Models\Client;

class ClientsSeeder extends BaseSeeder
{
    public function exec()
    {
        factory(Client::class, 200)->create();
    }
}