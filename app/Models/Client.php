<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AppAuthAccesToken
 * @property int $id
 * @property string $name
 * @property json $data
 * @package App\Models
 */
class Client extends Model
{
    const FIELD_DATE_OF_BIRTH = 'date_of_birth';
    const FIELD_GENDER = 'gender';
    const FIELD_HOBBY = 'hobby';

    const DATA_FIELDS = [
        self::FIELD_DATE_OF_BIRTH,
        self::FIELD_GENDER,
        self::FIELD_HOBBY
    ];

    protected $casts = [
        'data' => 'array',
    ];
}