<?php

namespace App\Models;

use App\Models\Client;
use Carbon\Carbon;
use DB;

class ClientsFilter
{
    protected $baseQuery;

    public function  __construct($baseQuery = false)
    {
        if ($baseQuery) {
            $this->baseQuery = $baseQuery;
        } else {
            $this->baseQuery = Client::query();
        }
    }

    public function getClients($params)
    {
        self::filter($params);
        $clients = $this->baseQuery->get();
        $hobbies = config('hobbies');
        foreach ($clients as $client) {
            if (!isset($client->data['hobby'])) {
                continue;
            }
            $clientHobbies = [];
            $data = $client->data;
            foreach ($data['hobby'] as $key) {
                $clientHobbies[] = $hobbies[$key];
            }
            $data['hobby'] = $clientHobbies;
            $client->data = $data;
        }
        return $clients;
    }

    public function filter($params)
    {
        foreach ($params as $param => $value) {
            if (method_exists($this, 'filterBy'.$param)) {
                $this->{'filterBy'.$param}($this->baseQuery, $value);
            }
        }
    }

    public function filterByGender($query, $value)
    {
        $query->where('data->gender', $value);
        $this->baseQuery = $query;
    }

    public function filterByHobby($query, $value)
    {
        $hobbies = config('hobbies');
        $hobbyArray = [];
        foreach ($value as $hobby) {
            $hobbyArray[] = "'[".array_search($hobby, $hobbies)."]'";
        }
        $query->whereRaw("data->'hobby' @> ANY (ARRAY[".implode(',',$hobbyArray)."]::jsonb[])");
        $this->baseQuery = $query;
    }

    public function filterByGeo_location($query, $geoLocation)
    {
        $ul_lat = $geoLocation['nw']['lat'];
        $ul_lng = $geoLocation['nw']['lng'];
        $lr_lat = $geoLocation['se']['lat'];
        $lr_lng = $geoLocation['se']['lng'];
        $ll_lat = $lr_lat;
        $ll_lng = $ul_lng;

        $ur_lat = $ul_lat;
        $ur_lng = $lr_lng;

        $polygon = DB::select("SELECT ST_MakeEnvelope(".$ll_lng.",".$ll_lat.",".$ur_lng.",".$ur_lat.", 4326)")[0]->st_makeenvelope;
        $query->whereRaw("ST_CONTAINS('$polygon', geo_location)");
        $this->baseQuery = $query;
        return;
    }

    public function filterByAge($query, $value)
    {
        $date_to = isset($value['from']) ? Carbon::now()->subYears($value['from']) : false;
        $date_from = isset($value['to']) ? Carbon::now()->subYears($value['to']+1)->addDay()->startOfDay() : false;

        $query->where(function ($query) use ($date_to, $date_from) {
            $query
                ->when($date_to, function ($query) use ($date_to) {
                    $query->where('data->date_of_birth', '<=', $date_to);
                })
                ->when($date_from, function ($query) use ($date_from) {
                    $query->where('data->date_of_birth', '>=', $date_from);
                });
        });

        $this->baseQuery = $query;
    }
}