<?php

namespace App\Http\Controllers;

use App\Models\ClientsFilter;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use DB;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return 1;
    }

    public function findUsers(Request $request)
    {
        $params = $request->all();
        $filter = new ClientsFilter();
        $clients = $filter->getClients($params);
        return json_encode($clients);
    }
}
