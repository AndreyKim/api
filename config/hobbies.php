<?php

return [
    '1' => "hockey",
    '2' => "curling",
    '3' => "snowboarding",
    '4' => "skiing",
    '5' => "fishing",
    '6' => "cycling",
    '7' => "baseball",
    '8' => "basketball",
    '9' => "football",
];